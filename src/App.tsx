import { useState } from 'react';
import Node from './components/Node'
import { v4 as uuid } from "uuid";

function App() {
  const [id, setId] = useState(uuid());
  return (
    <>
      <header className='w-full text-center text-5xl font-mono border-b-2 p-6'>🌲Tree app🌲</header>
      <div className='p-6 items-center'>
        <Node
          key={id}
          id={id}
          isRoot={true}
          onDelete={(_: string) => {}}
        />
        <button className="bg-red-500 text-white p-3 rounded-xl h-fit w-fit text-xl my-2" onClick={() => setId(uuid())}>Reset</button>
      </div>
    </>
  )
}

export default App
