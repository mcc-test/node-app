import { ReactElement } from "react";

interface Props {
    className: string;
    onClick?: () => void;
    type?: "button" | "submit" | "reset";
    children?: ReactElement;
}
export default function SmallButton({className, onClick, type, children}: Props) {
    return <button className={`w-7 h-7 p-1 ${className} rounded-xl`} onClick={onClick} type={type ?? "button"}>{children}</button>
}