import { useState } from "react";
import SmallButton from "./SmallButton";
import Icon from "./Icon";
import { useForm } from "react-hook-form";

interface Props {
    value: string;

    isRoot: boolean;

    destroy: () => void;
    addNode: () => void;
}

type Inputs = {
    name: string;
}

export default function NodeLabel({ isRoot, value, destroy, addNode }: Props) {
    const [currentName, setCurrentName] = useState(value);

    const [isCreating, setIsCreating] = useState(true)
    const [isEditing, setIsEditing] = useState(false);

    const { register, handleSubmit, reset: resetForm } = useForm<Inputs>({
        defaultValues: {
            name: value,
        }
    });

    const deny = () => {
        resetForm({ name: currentName });
        setIsEditing(false);
    }

    const submit = (data: Inputs) => {
        isCreating && setIsCreating(false);
        setCurrentName(data.name);
        setIsEditing(false);
    }

    return (
        <>
            {
                isEditing || isCreating ?
                <>
                    <form onSubmit={handleSubmit(submit)} className="flex flex-row gap-2 items-center">
                        <input {...register("name", { required: true })} className="px-2 border-2" autoFocus></input>
                        <SmallButton className="bg-green-400" type="submit">
                            <Icon name="check"/>
                        </SmallButton>
                        {
                            !(isRoot && isCreating) && 
                            <SmallButton className="bg-red-500" onClick={isCreating ? destroy : deny}>
                                <Icon name="cross"/>
                            </SmallButton>
                        }
                    </form>
                </>
                : <>
                    <p>{currentName}</p>
                    <div className="flex flex-row gap-2 items-center">
                        <SmallButton className="bg-green-400" onClick={addNode}>
                            <Icon name="plus"/>
                        </SmallButton>
                        <SmallButton className="bg-yellow-400" onClick={() => setIsEditing(true)}>
                            <Icon name="pencil"/>
                        </SmallButton>
                        {
                            !isRoot && 
                            <SmallButton className="bg-red-500" onClick={destroy}>
                                <Icon name="trash"/>
                            </SmallButton>
                        }
                    </div>
                </>
            }
        </>
    );
}