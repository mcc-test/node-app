import { useState } from "react";
import { v4 as uuid } from "uuid";
import NodeLabel from "./NodeLabel";

interface Props {
    id: string;
    isRoot: boolean;
    onDelete: (id: string) => void;
}

export default function Node({id, onDelete, isRoot}: Props) {
    const [childrenNodes, setChildrenNodes] = useState(new Array<string>())

    const addChildNode = () => setChildrenNodes(() => {
        let nodes = [...childrenNodes];
        nodes.push(uuid());
        return nodes;
    })

    const destroy = () => {
        setChildrenNodes([]);
        onDelete(id);
    }

    return (
        <>
            <div className="my-2">
                <div className="relative flex flex-row justify-between gap-10 w-fit items-center rounded-xl border-2 p-2 border-blue-200">
                    <NodeLabel 
                        value="default node"
                        isRoot={isRoot}
                        destroy={destroy}
                        addNode={addChildNode}
                    />
                </div>
                <div className="mx-10">
                    {
                        childrenNodes.map(nodeId => 
                            <Node
                                key={nodeId}
                                id={nodeId}
                                onDelete={(id: string) => setChildrenNodes(childrenNodes.filter(x => x != id))}
                                isRoot={false}
                            />
                        )
                    }
                </div>
            </div>
        </>
    );
}