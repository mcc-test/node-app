interface Props {
    name: "trash" | "plus" | "pencil" | "check" | "cross";
}

export default function Icon({name}: Props) {
    return <img src={`src/assets/${name}.svg`}/>
}